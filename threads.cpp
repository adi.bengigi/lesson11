#include "threads.h"

using namespace std;

void I_Love_Threads()
{
	cout << "I Love Threads" << endl;
}

void call_I_Love_Threads()
{
	thread t(I_Love_Threads);
	t.join();
}

void printVector(vector<int> primes)
{
	for (auto i = primes.begin(); i != primes.end(); ++i)
		cout << *i << endl;
}

void getPrimes(int begin, int end, vector<int>& primes)
{
	std::vector<bool> is_prime(end + 1, true);
	for (int i = 2; i*i <= end; ++i)
	{
		for (int j = i * i; j <= end; j += i)
		{
			is_prime[j] = false;
		}
	}

	for (int i = begin; i <= end; ++i)
	{
		if (is_prime[i]) {
			primes.push_back(i);
		}
	}
}

vector<int> callGetPrimes(int begin, int end)
{
	vector<int> primes;
	thread t(getPrimes, ref(begin), ref(end), ref(primes));
	t.join();

	return primes;
}

void writePrimesToFile(int begin, int end, ofstream & file)
{
	std::vector<bool> is_prime(end + 1, true);

	is_prime[1] = false;

	for (int i = 2; i*i <= end; ++i)
	{
		for (int j = i * i; j <= end; j += i)
		{
			is_prime[j] = false;
		}
	}

	for (int i = begin; i <= end; ++i)
	{
		if (is_prime[i]) {
			file << i << endl;
		}
	}
}

void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	int range_length = (end - begin + 1) / N;
	int temp_end, temp_begin = begin;

	ofstream file;
	file.open(filePath);

	auto start = high_resolution_clock::now(); //start timing

	for (size_t i = 0; i < N; i++)
	{
		temp_end = (temp_begin - 1) + range_length;
		thread t(writePrimesToFile, ref(temp_begin), ref(temp_end), ref(file));
		t.join();

		temp_begin += range_length;
	}

	auto stop = high_resolution_clock::now(); // end timing

	file.close();

	auto duration = duration_cast<milliseconds>(stop - start);

	cout << "Time taken: " << duration.count() << " milliseconds" << endl;
}
